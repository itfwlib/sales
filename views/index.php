<div class="btn-group" id="btn-group">
	<a href="<?php echo site_url('sales/createSales') ?>" class="btn btn-success">
		<span class="fa fa-plus"></span> Tambah Penjualan
		
	</a>
</div>

<?php echo $this->template->cardOpen('Penjualan');?>

<?php echo $this->template->cardBodyOpen();?>
	<table class="table table-hover table-bordered" id="salesTable">
		<thead>
			<tr>
				<?php foreach ($salesColumn as $key => $value): ?>
					<th>
						<?php echo ucwords(str_replace('_',' ',$value)) ?>
					</th>
				<?php endforeach ?>
			</tr>
		</thead>
	</table>
<?php echo $this->template->cardBodyClose();?>

<script type="text/javascript">
	$("#salesTable").DataTable({
		serverSide		: true,
		processing		: true,
		ajax 			: {
			url 		: "<?php echo site_url('sales/loadDatatable')?>",
			type 		: "POST"
		},
		columns 		: [
			<?php foreach ($salesColumn as $key => $value): ?>
				{data:"<?php echo $value ?>"},
			<?php endforeach ?>
		]
 	});
 	$("#salesTable_filter").append($("#btn-group"));
</script>