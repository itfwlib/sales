<!--
	# Awal 
	- inisialisasi select2, 
	- trigger change di $("#item_id").select2().trigger('change');

	# Form Penjualan 
		Untuk debugging form penjualan 		: CTRL + K + 5 
		Ketika selector item dirubah, akan masuk fungsi $("#item_id").on('change');
-->
<style type="text/css">
	.readonly { 
		background-color: #fff !important;
	}
	.error 		{
		color: red;
	}
</style>

<!-- row form -->
<div class="row">
	<div class="col-md-12">
		<?php echo $this->template->cardOpen('Tambah Penjualan');?>

		<?php echo $this->template->cardBodyOpen();?>
			<form method="POST" action="#" class="form-horizontal" id="form-add-sales">
				<!-- Item Selector -->
					<div class="form-group">
						<label class="control-label col-sm-2"><?php echo $config['label']['choose_item'] ?></label>
						<div class="col-sm-10">
							<select name="<?php echo $config['columns']['item']['item_id'] ?>" id="<?php echo $config['columns']['item']['item_id'] ?>" class="form-control select2" id="item_id">
								<?php foreach ($itemData as $key => $value): ?>
									<option value="<?php echo $value->$itemIDAlias ?>"><?php echo $value->$itemAlias ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				<!-- Item Selector -->

				<!-- Sisa Stok -->
					<div class="form-group">
						<label class="control-label col-sm-2">Sisa Stok</label>
						<div class="col-sm-10">
							<input type="text" class="form-control readonly" id="item_stock" name="item_stock" readonly>
						</div>
					</div>
				<!-- Sisa Stok -->

				<!-- Harga Barang -->
					<div class="form-group">
						<label class="control-label col-sm-2">Harga Barang</label>
						<div class="col-sm-10">
							<input type="text" class="form-control readonly" id="item_price" name="item_price" readonly>
						</div>
					</div>
				<!-- Harga Barang -->
				

				<!-- Jumlah Jual -->
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Jumlah Jual</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="qty" name="qty">
						</div>
					</div>
				<!-- Jumlah Jual -->


				<div class="form-group">
					<label class="control-label col-sm-2"></label>
					<div class="col-sm-10">
						<button class="btn btn-success pull-right" id="add-item" type="button">
							<span class="fa fa-plus"></span> Tambah Barang
						</button>
					</div>
				</div>
			</form>
		<?php echo $this->template->cardBodyClose();?>
	</div>
</div>

<!-- row detail -->
<div class="row">
	<div class="col-md-12">
		<?php echo $this->template->cardOpen('Detail Penjualan');?>
		
		<?php echo $this->template->cardBodyOpen();?>
		
		<?php echo $this->template->cardBodyClose();?>
	</div>
	
</div>


<script type="text/javascript">
	var itemArray 		= <?php echo json_encode($itemData) ?>;
	var select2 		= $(".select2").select2();
	var cart			= [];
	var selectedItems;
	var selectedItemKeys;

	/* Validating form */
	var addSalesValidation = $("#form-add-sales").validate({
		rules 		: {
			item_stock 		: {
				required 	: true,
				min 		: 1
			},
			qty 			: {
				required 	: true,
				min 		: 1,
				number		: true,
			}
		},
		messages 			: {
			item_stock 		: {
				required 	: "Sisa barang tidak mencukupi.",
				min 		: "Sisa barang tidak mencukupi."
			},
			qty 			: {
				required 	: "Jumlah penjualan harus diisi.",
				min 		: "Jumlah penjualan tidak bisa kurang dari 0",
			}	
		}
	});

	let findItem = (item_id = null) => {
		var findRows;
		$.each(itemArray,(key,rows)=>{
			if(rows.<?php echo $config['columns']['item']['item_id'] ?> == item_id){
				selectedItemKeys 		= key;
				findRows 				= rows;
			}
		});
		return findRows;
	}

	let generateError = (messages) => {
		return "<div class=\"error generated\">"+messages+"</div>";
	}

	let saveDetail = () => {
		itemID 				= $("#item_id").val();

		if(typeof cart[itemID] == 'undefined'){
			qty 	= Math.round($("#qty").val());
		}else{
			qty 	= cart[itemID].qty + Math.round($("#qty").val());
		}

		// Validasi Stok
		currentStock 		= selectedItems.<?php echo $config['columns']['item']['item_stock'] ?> 
		$(".generated").remove();
		if((currentStock - qty) < 0 ){
			error 		= generateError("Stok tidak mencukupi");
			$(error).insertAfter($("#qty"));
			return false;
		}

		// Tambah Item ke stok
		cart[item_id] 		= {
			"<?php echo $config['columns']['item']['item_id'] ?>" 		: itemID,
			"<?php echo $config['columns']['item']['item_price'] ?>" 	: selectedItems.<?php echo $config['columns']['item']['item_price'] ?>,
			"<?php echo $config['columns']['item']['item_name'] ?>" 	: selectedItems.<?php echo $config['columns']['item']['item_name'] ?>,
			"qty" 														: qty
		};

		return cart;
	}

	$(document).ready(function(){
		/* Item selector on change*/
		$("#item_id").on('change',()=>{
			item_id 			= $("#item_id").val();
			$.each(itemArray,(column,rows) => {
				if(rows.<?php echo $config['columns']['item']['item_id'] ?> == item_id){
					selectedItems 		= rows;
					currentStock 		= rows.<?php echo $config['columns']['item']['item_stock'] ?>;
					price 				= rows.<?php echo $config['columns']['item']['item_price'] ?>;
					if(currentStock <= 0){
						$("#item_stock").parent().parent().addClass('has-error');
						$("#add-item").attr('disabled','true');
					}else{
						$("#add-item").removeAttr('disabled');
						$("#item_stock").parent().parent().removeClass('has-error');
					}
					$("#item_stock").val(currentStock);
					$("#item_price").val(format_rp(price));
				}
			});
		});
		$("#item_id").select2().trigger('change');

		$("#add-item").click(()=>{
			var validate 		= addSalesValidation.form();
			if(!validate){
				return false;
			}
			saveDetail();
		});
	});

</script>