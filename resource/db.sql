/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.4.6-MariaDB : Database - itfw
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `sales_code` varchar(30) NOT NULL,
  `sales_date` date DEFAULT NULL,
  `sales_total` int(11) DEFAULT NULL,
  PRIMARY KEY (`sales_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `sales_detail` */

DROP TABLE IF EXISTS `sales_detail`;

CREATE TABLE `sales_detail` (
  `sales_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_code` varchar(30) DEFAULT NULL,
  `item_id` varchar(30) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`sales_detail_id`),
  KEY `sales_code` (`sales_code`),
  CONSTRAINT `sales_detail_ibfk_1` FOREIGN KEY (`sales_code`) REFERENCES `sales` (`sales_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
