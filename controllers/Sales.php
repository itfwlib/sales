<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'modules/core/controllers/Controller.php';

class Sales extends Controller {
	protected $_config;
	public function __construct(){
		parent::__construct();

		# Read Config Documentation https://gitlab.com/itfwlib/core/wikis/getConfig
		$this->_config 	= $this->getConfig('sales');
	}
	public function index(){
		$this->using('datatable');
		$config 		= $this->_config;
		$salesColumn 	= $config['columns']['sales'];
		// $this->template->load(DEF_TEMPLATE_INSIDE,'index',get_defined_vars());
		$this->dispatch(DEF_TEMPLATE_INSIDE,'index',get_defined_vars());
	}
	public function loadDatatable(){
		$post 			= $this->input->post();
		$this->load->model('Sales_model');

		$params 		= array(
			'draw' 		=> $post['draw'],
			'limit' 	=> $post['length'],
			'offset' 	=> $post['start'],
			'search' 	=> $post['search']['value'],
			'order' 	=> array(
				'columnName' 	=> $post['columns'][ $post['order'][0]['column'] ]['data'],
				'dir' 			=> $post['order'][0]['dir'],
			)
		);


		$fetch 					= $this->Sales_model->loadList(null,'LIST_ALL',$params,$this->_config);
		$dataTableResponse 		= array(
			'draw' 				=> $post['draw'],
			'data' 				=> $fetch->data->rows,
			'recordsFiltered' 	=> $fetch->data->recordsTotal,
			'recordsTotal' 		=> $fetch->data->recordsTotal
		);
		echo json_encode($dataTableResponse);
	}
	public function loadList(){
		$this->load->model('Sales_model');
		$params['limit'] 		= 10;
		$params['offset'] 		= 0;
		$params['search'] 		= 'Waw';
		$params['order'] 		= array(
			'columnName' 		=> 'sales_code',
			'dir' 				=> 'ASC'
		);
		$result 		= $this->Sales_model->loadList(null,'LIST_ALL',$params);

		echo '<pre>';
		print_r($result);
		echo '</pre>';
		die();
	}
	public function createSales(){
		$this->using('select2');
		$this->using('jquery.validate');
		$this->load->library('cart');
		/*Get item data*/
		$this->load->model($this->_config['tables']['item'].'/'.$this->_config['tables']['item'].'_model','itemModel');

		$config 			= $this->_config;
		$itemData 			= $this->itemModel->loadList()->data;
		$itemAlias 			= $config['columns']['item']['item_name'];
		$itemIDAlias 		= $config['columns']['item']['item_id'];

		$this->dispatch(DEF_TEMPLATE_INSIDE,'add_sales',get_defined_vars());
	}

}

/* End of file Sales.php */
/* Location: ./application/modules/sales/controllers/Sales.php */