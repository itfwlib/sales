<?php

class Config{
	public static function getConfig(){
		return array(
			# Daftar table yang harus ada.
			'tables' 		=> array(
				'sales' 		=> 'sales',
				'sales_detail' 	=> 'sales_detail',
				'item' 			=> 'item'
			),
			# Columns yang ada didalam table
			'columns' 		=> array(
				'sales' 	=> array(
					'sales_code' 		=> 'sales_code',
					'sales_date' 		=> 'sales_date',
					'sales_total' 		=> 'sales_total',
				),
				
				'sales_detail' 		=> array(
					'sales_detail_id' 	=> 'sales_detail_id',
					'sales_code' 		=> 'sales_code',
					'item_id' 			=> 'item_id',
					'qty' 				=> 'qty',
					'price' 			=> 'price'
				),

				# Disesuaikan dengan table yang menyimpan barang.
				'item' 				=> array(
					'item_id' 			=> 'item_id',
					'item_name' 		=> 'item_name',
					'item_price' 		=> 'item_price',
					'item_stock' 		=> 'item_stock'
				)
			),

			# label yang akan ditampilkan di interface
			'label' 	=> array(
				'choose_item' 		=> 'Pilih Barang',
				'item_id' 			=> 'ID Barang',
				'item_name' 		=> 'Nama Barang',
				'item_price' 		=> 'Harga Barang',
				'item_stock' 		=> 'Sisa stok barang'
			)
		);
	}
}