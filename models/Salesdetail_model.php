<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesdetail_model extends CI_Model {

	public function loadList($id,$mode = 'LOAD_ALL',$params = array()){
		$result 			= $this->general_model->result();
	
		/* Beta test improving performance ? */
		$this->db->select('*,COUNT(*) as totalRecords');
		
		/* Ordering */
		if(isset($params['order'])){
			$this->db->order_by($params['order']['columnName'],$params['order']['dir']);
		}
	
		/* Searching */
		if(isset($params['search']) && !empty($params['search'])){
			// $this->db->or_like('columnName',$params['search']);
		}
	
		/* limit offset */
		if(isset($params['limit'])){
			$this->db->limit(intval($params['limit']));
		}
		if(isset($params['offset'])){
			$this->db->offset(intval($params['offset']));
		}
	
		/* ---- EXECUTE QUERY ---- */
		$rows 			= $this->db->get('sales_detail')->result();
	
		/* Get total records from query & unset it from rows */
		foreach ($rows as $key => $value) {
			$totalRecords 		= $value->totalRecords;
			unset($rows[$key]->totalRecords);
		}
	
		$resData 				= new stdClass;
		$resData->rows 			= $rows;
		$resData->recordsTotal	= $totalRecords;
	
		$result->data 			= $resData;
		return $result;
	}

}

/* End of file Salesdetail_model.php */
/* Location: ./application/modules/sales/models/Salesdetail_model.php */